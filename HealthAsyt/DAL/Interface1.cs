﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthAsyt.DAL
{
    public interface IrxDAL
    {
        void AddNewRX(DateTime appointmentDate, string doctorName, string patientName, string symptoms);
        List<RxDAL> ViewRX();
        RxDAL ViewRxById(int rxId);
    }
}
