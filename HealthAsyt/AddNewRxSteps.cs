﻿using System;
using TechTalk.SpecFlow;

namespace HealthAsyt
{
    [Binding]
    public class AddNewRxSteps
    {
        [Given(@"patient name , Symptoms, Doctor name, AppointmentDate")]
        public void GivenPatientNameSymptomsDoctorNameAppointmentDate()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"no data entered by user")]
        public void GivenNoDataEnteredByUser()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"Click on save")]
        public void WhenClickOnSave()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"Success message should display ""(.*)"" with OK button")]
        public void ThenSuccessMessageShouldDisplayWithOKButton(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"on clicking on ""(.*)"" take user to Home page")]
        public void ThenOnClickingOnTakeUserToHomePage(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"Saved Rx should be displayed in Home page")]
        public void ThenSavedRxShouldBeDisplayedInHomePage()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"error message should display ""(.*)""")]
        public void ThenErrorMessageShouldDisplay(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"user should be retained in same page")]
        public void ThenUserShouldBeRetainedInSamePage()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
