﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using HealthAsyt.Service;
using HealthAsyt.DAL;

namespace HealthAsyt
{
    [TestFixture]
    public class TestCreatNewRX
    {
        [TestCase]

        public void ProvidePatientDetails()
        {
            RxService rx = new RxService();
            var result = rx.AddNewRX("dr Rashmi", "Harshitha", "Fever", DateTime.Now);
            Assert.AreEqual(result, "New Rx created successfully");
        }
        [TestCase]
        public void CheckDrDetails()
        {
            RxService rx = new RxService();
            var result = rx.AddNewRX("", "Harshitha", "Fever", DateTime.Now);
            Assert.AreEqual(result, "Please enter Dr Name");
        }

        [TestCase]
        public void CheckPatientDetails()
        {
            RxService rx = new RxService();
            var result = rx.AddNewRX("Rashmi", "", "Fever", DateTime.Now);
            Assert.AreEqual(result, "Please enter Patient Name");
        }

        [TestCase]
        public void CheckPatientsymptomsDetails()
        {
            RxService rx = new RxService();
            var result = rx.AddNewRX("Rashmi", "Sunil", "", DateTime.Now);
            Assert.AreEqual(result, "Please enter Patient symptoms");
        }

        [TestCase]
        public void CheckViewRxDetails()
        {
            RxService rx = new RxService();
            var result = rx.ViewAll().Count;
            Assert.Greater(result, 0);
        }

        [TestCase]
        public void CheckViewRxDetailsById()
        {
            Moq.Mock<DAL.IrxDAL> moq = new Moq.Mock<DAL.IrxDAL>();
            RxDAL rx = new RxDAL() { RxId = 1, AppointmentDate = DateTime.Now, DoctorName = "dr pavan", PatientName = "Rashmi", Symptoms = "Fever" } ;
            moq.Setup(x => x.ViewRxById(1)).Returns(rx);
            RxService rxservice = new RxService();
            var result = rxservice.ViewRxById(1);
            Assert.AreEqual(result.GetType(), rx.GetType());
        }
    }
}
