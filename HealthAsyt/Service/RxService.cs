﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthAsyt.Service
{
    public class RxService
    {
        DAL.RxDAL dal = new DAL.RxDAL();

        public string AddNewRX(string doctorName, string patientName, string symptoms, DateTime appointmentDate)
        {
            if (IsValid(doctorName))
            {
                return "Please enter Dr Name";
            }
            else if (IsValid(patientName))
            {
                return "Please enter Patient Name";
            }
            else if (IsValid(symptoms))
            {
                return "Please enter Patient symptoms";
            }
            else
            {
                dal.AddNewRX(appointmentDate, doctorName, patientName, symptoms);
                return "New Rx created successfully";
            }

        }

        public List<DAL.RxDAL> ViewAll()
        {
            return dal.ViewRX();
        }

        public virtual DAL.RxDAL ViewRxById(int rxId)
        {
            return dal.ViewRxById(rxId);
        }

        public bool IsValid(string value)
        {
            return string.IsNullOrEmpty(value);
        }
    }

}